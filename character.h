//
// Created by kagounard on 14/02/19.
//

#ifndef TFOT_CHARACTER_H
#define TFOT_CHARACTER_H

#include <iostream>

class Character
{
public:
    Character();
    int         m_health;
    std::string m_name;
    std::string m_type;
    ~Character();
};

#endif //TFOT_CHARACTER_H
